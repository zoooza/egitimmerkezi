﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EgitimMerkezi
{
   public class SliderMetadata
    {
        [Required]
        [Display(Name = "Slider Yazısı")]
        public string SliderText { get; set; }
    }

    public class DuyuruMetadata
    {

        [StringLength(255)]
        [Required]
        [Display(Name = "Duyuru Başlığı")]
        public string DuyuruBaslik { get; set; }

        [Required]
        public string Duyuru { get; set; }
    }
}